# Mobile Detection System
Project for the System Programming course at Politecnico di Torino using ESP32.

A people monitoring system has been implemented for an arbitrary area delimited by a variable number of ESP32 placed at the corners. Tracking takes place by intercepting the 802.11 broadcast packets, containing the probe requests, sent by the devices within the perimeter (laptops and smartphones).
