#include "sync.h"

void sync_service(char* serverIP) {
   ip_addr_t addr;
   sntp_setoperatingmode(SNTP_OPMODE_POLL);
   //inet_pton(AF_INET, CONFIG_SNTP_SERVER, &addr);
   //printf("CONFIG_SNTP_SERVER : %s", CONFIG_SNTP_SERVER);
   inet_pton(AF_INET, serverIP, &addr);
   sntp_setserver(0, &addr);
   sntp_init();
}
