#include <mbedtls/platform.h>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/debug.h>
#include <mbedtls/entropy.h>
#include <mbedtls/error.h>
#include <mbedtls/net.h>
#include <mbedtls/ssl.h>
#include <mbedtls/debug.h>
#include <errno.h>

#include <esp_log.h>
#include <string.h>
#include <stdio.h>
#include "packet.h"
#include "sniffer.h"

void secure_sending_service(char* serverIP);
