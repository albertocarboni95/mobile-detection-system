#ifndef SNIFFER_H
#define SNIFFER_H
#include "packet.h"


void sniffer_service(void);
void wifi_promiscuous_callback(void *buf, wifi_promiscuous_pkt_type_t type);
void channelHop(void);
unsigned int sniffer_get_current();

#endif
