#include "sniffer.h"


#define TIME 1000

static const char *TAG = "sniffer";

uint8_t wifi_channel = 1;
uint32_t current;
uint32_t found;
uint8_t monitor_flag = 0;
time_t now;

/* Filter out the common ESP32 MAC */
static const uint8_t esp_module_mac[32][3] = {
    {0x54, 0x5A, 0xA6}, {0x24, 0x0A, 0xC4}, {0xD8, 0xA0, 0x1D}, {0xEC, 0xFA, 0xBC},
    {0xA0, 0x20, 0xA6}, {0x90, 0x97, 0xD5}, {0x18, 0xFE, 0x34}, {0x60, 0x01, 0x94},
    {0x2C, 0x3A, 0xE8}, {0xA4, 0x7B, 0x9D}, {0xDC, 0x4F, 0x22}, {0x5C, 0xCF, 0x7F},
    {0xAC, 0xD0, 0x74}, {0x30, 0xAE, 0xA4}, {0x24, 0xB2, 0xDE}, {0x68, 0xC6, 0x3A},
};


void wifi_promiscuous_callback(void *buf, wifi_promiscuous_pkt_type_t type) {
   Packet packet;
	uint32_t i,j;
	uint8_t ssid_len;
	uint8_t _subtype;
	wifi_promiscuous_pkt_t *rxBuffer = (wifi_promiscuous_pkt_t *)buf; //wifi_promiscuous_pkt_t is the payload
	uint16_t probe_payload_len = rxBuffer->rx_ctrl.sig_len;				//passed to 'buf' parameter of prosmiscuous
	packet.timestamp = (unsigned int)time(&now);								// mode RX callback. Has 2 members:
	packet.channel = rxBuffer->rx_ctrl.channel;								// rx_ctrl(metadata header) and payload
	packet.rssi = rxBuffer->rx_ctrl.rssi;
	_subtype = rxBuffer->payload[0];
	if(_subtype == 0x40){
		for (i = 10, j = 0; j < 6; j++,i++){
			packet.mac[j] = rxBuffer->payload[i];
		}
		for (int i = 0; i < 32; ++i) { //filter out esp32 probe requests
			if (!memcmp(packet.mac, esp_module_mac[i], 3))
				return;
		}
		if(probe_payload_len > 0){
			if (rxBuffer->payload[24] == 0x00){
				ssid_len = (uint8_t)rxBuffer->payload[25];
				if(ssid_len>0){
					for(i = 26,j = 0;j < ssid_len; i++,j++){
						packet.ssid[j] = (char)rxBuffer->payload[i];
						if (packet.ssid[j] == ' ')
							packet.ssid[j] = '_';
					}
					packet.ssid[j] = '\0';
				}
				else{
					//broadcast
					packet.ssid[0] = '\0';
					strncpy(packet.ssid, "Broadcast", 10);
				}
			}
			else{
					strncpy(packet.ssid, "STRANGE", 10);
					//packet.ssid[0]='\0';
			}
	  }
		else{
				strncpy(packet.ssid, "STRANGE", 10);
			//packet.ssid[0]='\0';
		}
		if (monitor_flag){
			printf("%u CHAN:%u RSSI:%d MAC:%02x:%02x:%02x:%02x:%02x:%02x SSID:%s\n",
							packet.timestamp, packet.channel, packet.rssi,
							packet.mac[0], packet.mac[1], packet.mac[2], packet.mac[3], packet.mac[4], packet.mac[5],
							packet.ssid);

			if(current<DIM){
				packet_add(current, packet);
				current++;
			}
			found++;
		}
	}
}

unsigned int sniffer_get_current(){
	return current;
}

/*static void wifi_start_sniffing(void){
	ESP_LOGI(TAG, "wifi_start_sniffing()");
  ESP_ERROR_CHECK(esp_wifi_set_promiscuous(true));
}
static void wifi_stop_sniffing(void){
	ESP_LOGI(TAG, "wifi_stop_sniffing()");
	ESP_ERROR_CHECK(esp_wifi_set_promiscuous(false));
}
void channelHop(void){
	wifi_channel = (wifi_channel % 13) + 1;
	esp_wifi_set_channel(wifi_channel, WIFI_SECOND_CHAN_NONE);
}*/

void sniffer_service(void) {
	current = found = 0;
	printf("starting monitor mode, starting saving packets...\n");
	monitor_flag = 1;
	vTaskDelay(TIME*15/ portTICK_PERIOD_MS);
	printf("stopping monitor mode, stopping saving packets\n");
	monitor_flag = 0;
	//channelHop();
}
