#ifndef PACKET_H
#define PACKET_H

#include "freertos/FreeRTOS.h"
#include "freertos/ringbuf.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "string.h"
#define DIM 500
#define TIME 1000

typedef struct{
	uint32_t timestamp;
	uint8_t channel;
	int8_t rssi;
	uint8_t mac[6];
	char ssid[33];
} Packet;

void packet_add(uint16_t index, Packet p);
Packet packet_get(uint16_t index);


#endif
