#include "s_network.h"
// embedded binary data
extern const uint8_t ca_cer_start[] 		asm("_binary_ca_cer_start");
extern const uint8_t ca_cer_end[] 			asm("_binary_ca_cer_end");
extern const uint8_t espuser_cer_start[]  asm("_binary_espuser_cer_start");
extern const uint8_t espuser_cer_end[]    asm("_binary_espuser_cer_end");
extern const uint8_t espuser_key_start[]  asm("_binary_espuser_key_start");
extern const uint8_t espuser_key_end[]    asm("_binary_espuser_key_end");

#define SERVER_NAME "192.168.1.8"
#define SERVER_PORT CONFIG_PORT
static char tag[] = "secure_sending_service";
static char errortext[256];

static void my_debug(void *ctx, int level, const char *file, int line, const char *str) {
   (void) level;
   (void) ctx;
   printf("%s:%04d: %s", file, line, str);
}

void secure_sending_service(char* serverIP) {
   ESP_LOGD(tag, "--> callhttps\n");
   mbedtls_net_context server_fd;
   mbedtls_entropy_context entropy;
   mbedtls_ctr_drbg_context ctr_drbg;
   mbedtls_ssl_context ssl;
   mbedtls_ssl_config conf;
   mbedtls_x509_crt usercert;
   mbedtls_x509_crt cachain;
   mbedtls_pk_context pkey;
   int ret;
   int len;
   int devnum = CONFIG_DEVICE_NUM;
   unsigned int i = 0;
   char *pers = "ssl_client1";
   unsigned char buf[1024];

   //initialize mbedtls components
   mbedtls_net_init(&server_fd);
   mbedtls_ssl_init(&ssl);
   mbedtls_ssl_config_init(&conf);
   mbedtls_ctr_drbg_init(&ctr_drbg);
   mbedtls_entropy_init(&entropy);
   mbedtls_x509_crt_init(&usercert);
   mbedtls_x509_crt_init(&cachain);
   mbedtls_pk_init(&pkey);

   //load certificates and private pkey
   ret = mbedtls_x509_crt_parse(&cachain, ca_cer_start, ca_cer_end - ca_cer_start);
   //printf("PARSE CACHAIN %d\n", ret);
   ret = mbedtls_x509_crt_parse(&usercert, espuser_cer_start, espuser_cer_end - espuser_cer_start);
   //printf("PARSE CACERT %d\n", ret);
   ret = mbedtls_pk_parse_key(&pkey, espuser_key_start, espuser_key_end - espuser_key_start, NULL, 0);
   //printf("PARSE KEY %d\n", ret);
   //set the random number generator
   ret = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, (const unsigned char *) pers, strlen(pers));
   if (ret != 0) {
    ESP_LOGE(tag, " failed\n ! mbedtls_ctr_drbg_seed returned %d\n", ret);
    goto serve_exit;
   }
   //Setting up the connection
   ret = mbedtls_net_connect(&server_fd, serverIP, SERVER_PORT, MBEDTLS_NET_PROTO_TCP);
   if (ret != 0) {
      ESP_LOGE(tag, " failed\n ! mbedtls_net_connect returned %d\n\n", ret);
      goto serve_exit;
   }
   // prepare configuration
   ret = mbedtls_ssl_config_defaults(&conf, MBEDTLS_SSL_IS_CLIENT, MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT);
   if (ret != 0) {
    ESP_LOGE(tag, " failed\n ! mbedtls_ssl_config_defaults returned %d\n\n", ret);
    goto serve_exit;
   }
   //require double autentication
   mbedtls_ssl_conf_authmode(&conf, MBEDTLS_SSL_VERIFY_REQUIRED);
   //configure CA chain and client certificates
   mbedtls_ssl_conf_ca_chain(&conf, &cachain, NULL);
   mbedtls_ssl_conf_own_cert(&conf, &usercert, &pkey);
   //random engine
   mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);

   mbedtls_ssl_conf_dbg(&conf, my_debug, stdout);
   //mbedtls_debug_set_threshold(2); // Log at error only undefined reference

   ret = mbedtls_ssl_setup(&ssl, &conf);
   if (ret != 0) {
      mbedtls_strerror(ret, errortext, sizeof(errortext));
      ESP_LOGE(tag, "error from mbedtls_ssl_setup: %d - %x - %s\n", ret, ret, errortext);
      goto serve_exit;
   }
   ret = mbedtls_ssl_set_hostname(&ssl, "espserver");
   if (ret != 0) {
      mbedtls_strerror(ret, errortext, sizeof(errortext));
      ESP_LOGE(tag, "error from mbedtls_ssl_set_hostname: %d - %x - %s\n", ret, ret, errortext);
      goto serve_exit;
   }
   mbedtls_ssl_conf_read_timeout(&conf, 10000);
   //configure input output functions
   //mbedtls_ssl_set_bio(&ssl, &server_fd, mbedtls_net_send, mbedtls_net_recv, NULL);
   mbedtls_ssl_set_bio(&ssl, &server_fd, mbedtls_net_send, NULL, mbedtls_net_recv_timeout);

   //handshake
   printf("Performing the SSL/TLS handshake...\n");
   while ((ret = mbedtls_ssl_handshake(&ssl)) != 0) {
      if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE) {
         ESP_LOGE(tag, "mbedtls_ssl_handshake returned -0x%x", -ret);
         goto serve_exit;
      }
   }
   printf("SSL/TLS handshake successfully performed!\n");

   ESP_LOGI(tag, "Verifying peer X.509 certificate...");
   if ((ret = mbedtls_ssl_get_verify_result(&ssl)) != 0) {
      /* In real life, we probably want to close connection if ret != 0 */
      ESP_LOGW(tag, "Failed to verify peer certificate!");
      bzero((char *)buf, sizeof(buf));
      mbedtls_x509_crt_verify_info((char *)buf, sizeof(buf), "  ! ", ret);
      ESP_LOGW(tag, "verification info: %s", (char *)buf);
   }
   else {
      ESP_LOGI(tag, "Certificate verified.");
   }
   ESP_LOGI(tag, "Cipher suite is %s", mbedtls_ssl_get_ciphersuite(&ssl));

   //sprintf((char *)buf, requestMessage);
   //len = strlen((char *)buf);
   char macaddr[54];
   //writing devnum
   sprintf((char *) buf, "%d", devnum);
   len = strlen((char*)buf);
   mbedtls_ssl_write(&ssl, buf, len); //sending device number to server
   unsigned int total_packets = sniffer_get_current();
   for (i = 0; i < total_packets; i++ ) {
      snprintf(macaddr, 54, "%02x:%02x:%02x:%02x:%02x:%02x", packet_get(i).mac[0],
         packet_get(i).mac[1], packet_get(i).mac[2], packet_get(i).mac[3],
         packet_get(i).mac[4], packet_get(i).mac[5]);
      snprintf((char*)buf, 256,
         "%u DEVNUM=%d PROBE_REQ CHAN=%u RSSI=%d MAC=:%s SSID=%s\n",
         packet_get(i).timestamp, devnum, packet_get(i).channel,
         packet_get(i).rssi, macaddr, packet_get(i).ssid);
      len = strlen((char*)buf);
      ret = mbedtls_ssl_write(&ssl, buf, len);
      if (ret < 0) {
      mbedtls_strerror(ret, errortext, sizeof(errortext));
      ESP_LOGE(tag, "error from write: %d -%x - %s\n", ret, ret, errortext);
      goto serve_exit;
      }
  }
serve_exit:
   mbedtls_pk_free(&pkey);
   mbedtls_net_free(&server_fd);
   mbedtls_x509_crt_free(&usercert);
   mbedtls_x509_crt_free(&cachain);
   mbedtls_ssl_free(&ssl);
   mbedtls_ssl_config_free(&conf);
   mbedtls_ctr_drbg_free(&ctr_drbg);
   mbedtls_entropy_free(&entropy);
   if( ret == MBEDTLS_ERR_SSL_ALLOC_FAILED) {
      ESP_LOGE(tag, "NOMEM");
   }
   ESP_LOGI(tag, "All done");
}
