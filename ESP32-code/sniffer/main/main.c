#include "sniffer.h"
#include "network.h"
#include "sync.h"
#include "s_network.h"

char serverIP[16] = {0}; //with this string i pass the value to mdns(that sets its value), sync and sending that use its value
int count = 0;

void main_task() {
   while (1) {
      sniffer_service();
      //sending_service(serverIP);
      secure_sending_service(serverIP);
      printf("%d\n", count);
      count++;
  	   for (int countdown = 3; countdown >= 0; countdown--) {
  	      ESP_LOGI("idle", "%d... ", countdown);
  		   vTaskDelay(TIME / portTICK_PERIOD_MS);
      }
   }
}

void app_main() {
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);
  time_t now;
  //time(&now);
  //printf("HERE->TIME at BOOT:%u\n", (unsigned int)now);
  _wifi_init();
  start_mdns_service();
  resolve_mdns_host(CONFIG_MDNS_SERVER_NAME, serverIP);
  sync_service(serverIP);
  //strcpy(serverIP, "192.168.1.5");
  vTaskDelay(TIME / portTICK_PERIOD_MS);
  time(&now);
  printf("HERE->TIME:%u\n", (unsigned int)now);
  vTaskDelay(TIME / portTICK_PERIOD_MS);
  xTaskCreate(&main_task, "main_task", 8192, NULL, 5, NULL);
}
