#ifndef SYNC_H
#define SYNC_H

#include "apps/sntp/sntp.h"
#include "lwip/sockets.h"

void sync_service(char* serverIP);

#endif
