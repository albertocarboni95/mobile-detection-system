#ifndef NETWORK_H
#define NETWORK_H
#include "tcpip_adapter.h"
#include "lwip/sockets.h"
#include "lwip/opt.h"
#include "lwip/ip_addr.h"
#include "lwip/netif.h"
#include "packet.h"
#include "sniffer.h"
#include "mdns.h"

int sending_service(char* serverIP);
int network_service(void);
esp_err_t event_handler(void *ctx, system_event_t *event);
void _wifi_init(void);
void start_mdns_service();
void resolve_mdns_host(const char * host_name, char *ipadrr);

#endif
