#include "network.h"

static char *tag = "socket_client";
struct sockaddr_in saddr;
struct in_addr     sIPaddr;
uint16_t           tport_n, tport_h;
static EventGroupHandle_t wifi_event_group;
uint8_t flag = 0;
//uint8_t wifi_channel = 1;
const static int CONNECTED_BIT = BIT0;

esp_err_t wifi_event_handler(void *ctx, system_event_t *event) {
   switch (event->event_id) {
      case SYSTEM_EVENT_STA_START:
         esp_wifi_connect();
         break;
      case SYSTEM_EVENT_STA_GOT_IP:
         printf("Our IP address is " IPSTR "\n", IP2STR(&event->event_info.got_ip.ip_info.ip));
         printf("We have now connected to a station and can do things...\n");
         xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
         if (!flag) {
            ESP_ERROR_CHECK(esp_wifi_set_promiscuous_rx_cb(wifi_promiscuous_callback));
            ESP_ERROR_CHECK(esp_wifi_set_promiscuous(1));
         }
         break;
      case SYSTEM_EVENT_STA_DISCONNECTED:
         esp_wifi_connect();
         xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
         break;
      default:
         break;
  }
  return ESP_OK;
}

int sending_service(char* serverIP) {
   //resolve_mdns_host("ale2");
	int result;
	int s;
	unsigned int i = 0;
   int devnum = CONFIG_DEVICE_NUM;
   /* input IP address and port of server */
   result = inet_aton(serverIP, &sIPaddr);
   if (!result) {
      ESP_LOGE(tag, "Invalid address");
      return -1;
   }

   if (sscanf(CONFIG_PORT, "%" "hu", &tport_h)!=1) {
      ESP_LOGE(tag, "Invalid port number");
      return -1;
   }
   tport_n = htons(tport_h);

   /* create the socket */
   printf("Creating socket\n");
   s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
   if (s < 0) {
      ESP_LOGE(tag, "Socket creation failed!\n");
    	return -1;
   }
   printf("done. Socket fd number: %d\n",s);

    /* prepare address structure */
    bzero(&saddr, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_port   = tport_n;
    saddr.sin_addr   = sIPaddr;

    printf("Connecting to target address\n");
    result = connect(s, (struct sockaddr *) &saddr, sizeof(saddr));
    printf("result: %d\n",result);
    if (result < 0) {
    	ESP_LOGE(tag, "Connect failed!\n");
      close(s);
    	return -1;
    }
    printf("done.\n");
    char macaddr[54], buffer[256];
    write(s, &devnum, sizeof(devnum));  // I am device number <DEVICENUMBER>
    unsigned int total_packets = sniffer_get_current();
    for(i = 0; i < total_packets; i++ ){
      snprintf(macaddr, 54, "%02x:%02x:%02x:%02x:%02x:%02x", packet_get(i).mac[0], packet_get(i).mac[1],
              packet_get(i).mac[2], packet_get(i).mac[3], packet_get(i).mac[4],
              packet_get(i).mac[5]);
      snprintf(buffer, 256, "%u PROBE_REQ CHAN=%u RSSI=%d MAC=:%s SSID=%s\n",
  						packet_get(i).timestamp, packet_get(i).channel, packet_get(i).rssi, macaddr, packet_get(i).ssid);
      result = write(s, buffer, strlen(buffer));
      if (result < 0) {
         ESP_LOGE(tag, "Writing Error");
         close(s);
      	 return -1;
    }
  }
  printf("closing socket..\n");
  int err;
  if((err = close(s))< 0){
    printf("CLOSING ERROR: %d", err);
    return -1;
  }
  else
    printf("socket closed\n");
	return 0;
}

void _wifi_init(void) {
	ESP_LOGI(tag, "wifi_init()\n");
  tcpip_adapter_init();
  wifi_event_group = xEventGroupCreate();
  ESP_ERROR_CHECK(esp_event_loop_init(wifi_event_handler, NULL));
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  //esp_wifi_set_channel(wifi_channel, WIFI_SECOND_CHAN_NONE);
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
  wifi_config_t sta_config = {
   .sta = {
   .ssid = CONFIG_AP_SSID,
   .password = CONFIG_AP_PASSWD,
   .bssid_set = 0
    }
  };
  ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &sta_config));
  ESP_ERROR_CHECK(esp_wifi_start());
  ESP_LOGI(tag, "Waiting for wifi");
  xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
}

void start_mdns_service()
{
    //initialize mDNS service
    esp_err_t err = mdns_init();
    if (err) {
        printf("MDNS Init failed: %d\n", err);
        return;
    }
    //set hostname
    mdns_hostname_set("esp32");
    //set default instance
    mdns_instance_name_set("Ale's ESP32 Thing");
    ESP_LOGI(tag, "mdns service started");
}

void resolve_mdns_host(const char * host_name, char *ipadrr)
{
    printf("Query A: %s.local ", host_name);

    struct ip4_addr addr;
    addr.addr = 0;

    esp_err_t err = mdns_query_a(host_name, 7000,  &addr);
    if(err){
        if(err == ESP_ERR_NOT_FOUND){
            printf(" Host was not found!");
            return;
        }
        printf("Query Failed");
        return;
    }
    printf(IPSTR, IP2STR(&addr));
    char*tmp = ip4addr_ntoa(&addr);
    strncpy(ipadrr, tmp, 16);
    printf("IPADRR:%s", ipadrr);
}
