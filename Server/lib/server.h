#include <sys/wait.h>
#include <sys/stat.h>
#include "../lib/database.h"
#include "../lib/network.h"

#define RBUFLEN		128

/* FUNCTION PROTOTYPES */
void service(int s, char *buf, SSL *ssl);
void handler(int sig);
void config_params(int *numdevices, char *portnumber);
int file_exist(char *filename);
