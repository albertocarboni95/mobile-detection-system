#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sqlite3.h>

void _sqlite3_open(const char *filename, sqlite3 **ppDb);
void sql_create_table(sqlite3 *db, char *zErrMsg);
void sql_insert_tuples(sqlite3 *db, char *zErrMsg, FILE *fp, int esp_id);
void sql_execute_query(sqlite3 *db, char *zErrMsg);
void populate_db(FILE *fp, int esp_id);
void create_db();
