#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h> // inet_aton()
#include <sys/un.h> // unix sockets
#include <unistd.h>
#include <inttypes.h> // SCNu16
#include <openssl/ssl.h>
#include <openssl/err.h>

int create_socket();
void bind_socket(int s, struct sockaddr_in *sin, uint16_t lport_n);
void listen_socket(int s, int bklog);
int accept_connection(int conn_request_skt, struct sockaddr_in *ca);
void close_socket(int s);
void configure_context(SSL_CTX *ctx);
SSL_CTX *create_context();
void cleanup_openssl();
void init_openssl();
