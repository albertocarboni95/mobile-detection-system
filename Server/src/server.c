#include "../lib/server.h"
#include <string.h>

#define N 3

int main(int argc, char *argv[])
{
    int		conn_request_skt;	/* passive socket */
    uint16_t 	lport_n, lport_h;	/* port used by server (net/host ord.) */
    int		bklog;		/* listen backlog */
    int		childpid;		/* pid of child process */
    int		s;			/* connected socket */
    struct 	sockaddr_in	saddr;	/* server address */
    struct 	sockaddr_in 	caddr;
    int 	count = 0, numdevices;
    char	filename[128];
    FILE	*fp;
    char buf[RBUFLEN];		 /* reception buffer */
    char portnumber[32];

    config_params(&numdevices, portnumber);

    bklog = numdevices;

    /* input server port number */
    if (sscanf(portnumber, "%" SCNu16, &lport_h)!=1)
    {
	fprintf(stderr,"Invalid port number\n");
	exit(1);
    }
    lport_n = htons(lport_h);

    /* setup SSL */
    SSL_CTX *ctx;
    init_openssl();
    ctx = create_context();
    configure_context(ctx);

    /* create the socket */
    s = create_socket();

    /* bind the socket to any local IP address */
    bind_socket(s, &saddr, lport_n);

    /* listen */
    listen_socket(s, bklog);

    conn_request_skt = s;

    /* create signal handler */
    signal(SIGCHLD, handler);

    /*setup table*/
    sprintf(filename, "data/sniff.db");
    if(!file_exist(filename))
    	create_db();
    /* main server loop */
    for (;;)
    {
	/* accept next connection */
	s = accept_connection(conn_request_skt, &caddr);
	SSL *ssl = SSL_new(ctx);
        SSL_set_fd(ssl, s);
	count++;

	/* serve the client on socket s */
	if((childpid=fork())<0)
	{
	    fprintf(stderr,"fork() failed");
	    SSL_free(ssl);
	    close_socket(s);
	    exit(1);
	}
	else if (childpid == 0)
	{
	    /* child process */
	    service(s, buf, ssl);	/* enter service loop */
	    printf("==============================================\n");
	    exit(0);
	}

	if(count == numdevices)
	{
	    int i, status;

	    for(i=0; i<numdevices; i++)
		wait(&status);

	    for(i=1; i<=numdevices; i++)
	    {
		sprintf(filename, "data/sniff_%d.txt", i);
		if((fp = fopen(filename, "r"))==NULL)
		{
		    fprintf(stderr,"Error opening file %s\n", filename);
		    exit(-1);
		}
		populate_db(fp, i);
		fclose(fp);
	    }
	    count = 0;
	}
    }
    close_socket(conn_request_skt);
    SSL_CTX_free(ctx);
    cleanup_openssl();
}

int file_exist(char *filename)
{
	struct stat buffer;
  	return (stat (filename, &buffer) == 0);
}

void handler(int sig)
{
    pid_t pid;
    while ((pid = waitpid((pid_t)(-1), 0, WNOHANG)) > 0);
    //printf("PID %d exited\n", pid);
    signal(SIGCHLD, handler);
}

void config_params(int *numdevices, char *portnumber)
{
    FILE *fp;

    if((fp = fopen("param.conf", "r"))==NULL)
    {
	fprintf(stderr,"Error opening configuration file\n");
	exit(-1);
    }

    fscanf(fp, "Number of devices: %d\n", numdevices);
    fscanf(fp, "Server port number: %s\n", portnumber);

    fclose(fp);
}

/* Provides service on the passed socket */
void service(int s, char* buf, SSL *ssl)
{
    int	 n;
    FILE *fp;
    char filename[128], espidstr[N];
    int esp_id;

    if (SSL_accept(ssl) <= 0)
            ERR_print_errors_fp(stderr);

    SSL_read(ssl, espidstr, N*sizeof(char));
    esp_id = atoi(espidstr);

    printf("DEVNUM:%d\n", esp_id);

    sprintf(filename, "data/sniff_%d.txt", esp_id);
    if((fp = fopen(filename, "w"))==NULL)
    {
	fprintf(stderr,"Error opening file %s\n", filename);
	exit(-1);
    }

    for (;;)
    {
	n=SSL_read(ssl, buf, RBUFLEN-1);
  printf("%s\n", buf);
	if (n < 0)
	{
	   printf("Read error\n");
	   SSL_free(ssl);
	   close_socket(s);
	   printf("Socket %d closed\n", s);
	   break;
	}
	else if (n==0)
	{
	   printf("Connection closed by party on socket %d\n",s);
	   SSL_free(ssl);
	   close_socket(s);
	   break;
	}
       	else
	{
	    //printf("Received data from socket %03d\n", s);
	    buf[n]=0;
	    fprintf(fp, "%s", buf);
	}
    }
}
