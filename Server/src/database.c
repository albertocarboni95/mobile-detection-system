#include "../lib/database.h"

static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

void _sqlite3_open(const char *filename, sqlite3 **ppDb)
{
   int rc;

   rc = sqlite3_open(filename, ppDb);

   if( rc ) {
      fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(*ppDb));
      exit(-1);
   } else {
      fprintf(stderr, "Opened database successfully\n");
   }
}

void create_db()
{
    sqlite3 *db;
    char *zErrMsg = 0;

    _sqlite3_open("data/sniff.db", &db);
    sql_create_table(db, zErrMsg);
    sqlite3_close(db);
}

void sql_create_table(sqlite3 *db, char *zErrMsg)
{
   int rc;
   char *sql;

   sql = (char *)"CREATE TABLE SNIFF("  \
         "TIMESTAMP 	INT	NOT NULL," \
	 "ESPID 	INT	NOT NULL," \
	 "CHANNEL       INT    NOT NULL," \
         "MAC           CHAR(50)    NOT NULL," \
         "RSSI          INT     NOT NULL," \
         "SSID          CHAR(50) );";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

   if( rc != SQLITE_OK ){
   fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
   } else {
      fprintf(stdout, "Table created successfully\n");
   }
}

void sql_insert_tuples(sqlite3 *db, char *zErrMsg, FILE *fp, int esp_id)
{
   int rc;
   unsigned int timestamp;
   int chan, rssi, devnum;
   char mac[50], ssid[100], sql[200];


   //196180126 PROBE_REQ CHAN=1 RSSI=-74 MAC=:da:a1:19:26:e7:da SSID=Broadcast
   while((fscanf(fp, "%u DEVNUM=%d PROBE_REQ CHAN=%d RSSI=%d MAC=:%s SSID=%s",
	 &timestamp, &devnum, &chan, &rssi, mac, ssid))!= EOF)
   {
	sprintf(sql, "INSERT INTO SNIFF(TIMESTAMP, ESPID, CHANNEL,MAC,RSSI,SSID) VALUES(%u,%d, %d,'%s',%d,'%s');",
	timestamp,esp_id,chan,mac,rssi,ssid);
	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if( rc != SQLITE_OK )
	{
	    fprintf(stderr, "SQL error: %s\n", zErrMsg);
	    sqlite3_free(zErrMsg);
	}
   }
}

void sql_execute_query(sqlite3 *db, char *zErrMsg)
{
   int rc;
   char *sql;

   sql = (char *)"SELECT * from SNIFF";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

   if( rc != SQLITE_OK ){
   fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
   } else {
      fprintf(stdout, "Query submitted successfully\n");
   }
}

void populate_db(FILE *fp, int esp_id)
{
    sqlite3 *db;
    char *zErrMsg = 0;

    _sqlite3_open("data/sniff.db", &db);
    sql_insert_tuples(db, zErrMsg, fp, esp_id);
    sql_execute_query(db, zErrMsg);
    sqlite3_close(db);
}
