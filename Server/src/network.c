#include "../lib/network.h"

void init_openssl()
{ 
    SSL_load_error_strings();	
    OpenSSL_add_ssl_algorithms();
}

void cleanup_openssl()
{
    EVP_cleanup();
}

SSL_CTX *create_context()
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    method = SSLv23_server_method();

    ctx = SSL_CTX_new(method);
    if (!ctx) {
	perror("Unable to create SSL context");
	ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    return ctx;
}

void configure_context(SSL_CTX *ctx)
{
    SSL_CTX_set_ecdh_auto(ctx, 1);

    /* Set the key and cert */
    if (SSL_CTX_use_certificate_file(ctx, "certs/espserver.pem", SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, "certs/espserverkey.pem", SSL_FILETYPE_PEM) <= 0 ) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }
}

static void showAddr(char *str, struct sockaddr_in *a)
{
    char *p;
    
    p = inet_ntoa(a->sin_addr);
    printf("%s %s!",str,p);
    printf("%" SCNu16, ntohs(a->sin_port));
    printf("\n");
}

int create_socket()
{
	int s;
	printf("creating socket...\n");
    	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    	printf("done, int number %u\n",s);
	return s;
}

void bind_socket(int s, struct sockaddr_in *sin, uint16_t lport_n)
{
	struct 	sockaddr_in	saddr = *sin;

	bzero(&saddr, sizeof(saddr));
	saddr.sin_family      = AF_INET;
    	saddr.sin_port        = lport_n;
    	saddr.sin_addr.s_addr = INADDR_ANY;
    	showAddr((char *)"Binding to address", &saddr);
    	bind(s, (struct sockaddr *) &saddr, sizeof(saddr));
    	printf("done.\n");
}

void listen_socket(int s, int bklog)
{
	printf ("Listening at socket %d with backlog = %d \n",s,bklog);
    	listen(s, bklog);
    	printf("done.\n");
}

int accept_connection(int conn_request_skt, struct sockaddr_in *ca)
{
	int s;
	struct sockaddr_in caddr = *ca;
    	int addrlen;

	addrlen = sizeof(struct sockaddr_in);
	s = accept(conn_request_skt, (struct sockaddr *) &caddr, (socklen_t *) &addrlen);
	showAddr((char *)"Accepted connection from", &caddr);
	printf("new socket: %u\n",s);
	
	return s;
}

void close_socket(int s)
{
	close(s);
}
